#!/usr/bin/env python3

import asyncio
import json
import websockets

from typing import Any, Dict


async def socket_func(websocket, path: str) -> None:
    message: Dict[str, Any] = {
        "location": [0.0, 0.0]
    }
    await websocket.send(json.dumps(message))

if __name__ == "__main__":
    server = websockets.serve(socket_func, "localhost", 8765)
    asyncio.get_event_loop().run_until_complete(server)
    asyncio.get_event_loop().run_forever()
