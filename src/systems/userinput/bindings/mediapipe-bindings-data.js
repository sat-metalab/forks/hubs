import { paths } from "../paths";
import { xforms } from "./xforms";

const mediaPipeActionPrefs = {
    faceUp: {
      key: `faceUpAction`,
    },
    faceDown: {
      key: `faceDownAction`,
    },
    faceLeft: {
      key: `faceLeftAction`,
    },
    faceRight: {
      key: `faceRightAction`,
    },
    leftEye: {
        key: `leftEyeAction`,
    },
    rightEye: {
        key: `rightEyeAction`,
    },
    mouth: {
        key: `mouthAction`,
    },
};

// For: src/storage/store.js
const satellitePreferencesStore = {
  showMediaPipeDebugPanel: { type: "bool", default: false },
  faceUpAction:    { type: "string", default: "lookUp" },
  faceDownAction:  { type: "string", default: "lookDown" },
  faceLeftAction:  { type: "string", default: "lookLeft" },
  faceRightAction: { type: "string", default: "lookRight" },
  leftEyeAction:   { type: "string", default: "inspectSelf" },
  rightEyeAction:  { type: "string", default: "teleport" },
  mouthAction:     { type: "string", default: "spawnEmoji1" },
  mediaPipeDrawNose:  { type: "boolean", default: true },
  mediaPipeDrawEye:   { type: "boolean", default: true },
  mediaPipeDrawMouth: { type: "boolean", default: true },
  mediaPipeFaceUpTrigger:    { type: "number", default: 230 },
  mediaPipeFaceDownTrigger:  { type: "number", default: 210 },
  mediaPipeFaceLeftTrigger:  { type: "number", default: 170 },
  mediaPipeFaceRightTrigger: { type: "number", default: 190 },
  mediaPipeMouthCloseThreshold: { type: "number", default: 0.02 },
  mediaPipeEyeCloseThreshold:   { type: "number", default: 13.5 },
}

const bindingsData = {
  lookUp: {
    message: `Look Up`,
    bindings: [
      {
        dest: { value: paths.actions.cameraDelta },
        xform: xforms.add_vec2,
        src: {
          first: ``,
          second: paths.actions.cameraDelta,
        },
      },
    ],
  },

  lookDown: {
    message: `Look Down`,
    bindings: [
      {
        dest: { value: paths.actions.cameraDelta },
        xform: xforms.add_vec2,
        src: {
          first: ``,
          second: paths.actions.cameraDelta,
        },
      },
    ],
  },

  lookLeft: {
    message: `Look Left`,
    bindings: [
      {
        dest: { value: paths.actions.cameraDelta },
        xform: xforms.add_vec2,
        src: {
          first: ``,
          second: paths.actions.cameraDelta,
        },
      },
    ],
  },

  lookRight: {
    message: `Look Right`,
    bindings: [
      {
        dest: { value: paths.actions.cameraDelta },
        xform: xforms.add_vec2,
        src: {
          first: ``,
          second: paths.actions.cameraDelta,
        },
      },
    ],
  },

  teleport: {
    message: `Teleport`,
    bindings: [
      { dest: { value: paths.actions.startGazeTeleport }, xform: xforms.falling,  src:  { value: `` }},
      { dest: { value: paths.actions.stopGazeTeleport  }, xform: xforms.rising, src:  { value: `` }},
    ],
  },

  boost: {
    message: `Move faster`,
    bindings: [{
      dest: { value: paths.actions.boost },
      xform: xforms.copy,
      src: { value: `` },
      priority: 1
    }],
  },

  focusChat: {
    message: `Focus chat`,
    bindings: [{ dest: { value: paths.actions.focusChat }, xform: xforms.falling, src:  { value: `` }}],
  },

  focusChatCommand: {
    message: `Focus chat command (\`/\`)`,
    bindings: [{ dest: { value: paths.actions.focusChatCommand }, xform: xforms.falling, src:  { value: `` }}],
  },

  exitMirror: {
    message: `Exit camera mirror mode`,
    bindings: [{
      dest: { value: paths.actions.camera.exitMirror },
      xform: xforms.rising,
      src: { value: `` },
    }],
  },

  mediaExit: {
    message: `Exit media`,
    bindings: [{
      dest: { value: paths.actions.mediaExit },
      xform: xforms.falling,
      src: { value: `` },
    }],
  },

  toggleFly: {
    message: `Toggle Fly Mode`,
    bindings: [{ dest: { value: paths.actions.toggleFly }, xform: xforms.falling, src:  { value: `` }}],
  },

  toggleFreeze: {
    message: `Toggle Menus`,
    bindings: [{ dest: { value: paths.actions.toggleFreeze }, xform: xforms.falling, src:  { value: `` }}],
  },

  toggleUI: {
    message: `Toggle UI`,
    bindings: [{ dest: { value: paths.actions.toggleUI }, xform: xforms.falling, src:  { value: `` }}],
  },

  muteMic: {
    message: `Toggle Microphone Mute`,
    bindings: [{ dest: { value: paths.actions.muteMic }, xform: xforms.falling, src:  { value: `` }}],
  },

  inspectSelf: {
    message: `Inspect Avatar`,
    bindings: [
      { dest: { value: paths.actions.startInspectingSelf}, xform: xforms.falling,  src: { value: ``}},
      { dest: { value: paths.actions.stopInspecting     }, xform: xforms.rising, src: { value: ``}},
    ],
  },

  spawnEmoji0: {
    message: `Emoji 1`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji0 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji1: {
    message: `Emoji 2`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji1 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji2: {
    message: `Emoji 3`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji2 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji3: {
    message: `Emoji 4`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji3 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji4: {
    message: `Emoji 5`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji4 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji5: {
    message: `Emoji 6`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji5 }, xform: xforms.falling, src:  { value: `` }}],
  },

  spawnEmoji6: {
    message: `Emoji 7`,
    bindings: [{ dest: { value: paths.actions.spawnEmoji6 }, xform: xforms.falling, src:  { value: `` }}],
  },
};

function generateActionOptions(actionID, intl) {
    return Object.keys(bindingsData).map((key) => {
        return {
            value: key,
            text: intl.formatMessage({
                id: actionID,
                defaultMessage: bindingsData[key].message
            })
        }
    })
}

export { mediaPipeActionPrefs, bindingsData, satellitePreferencesStore, generateActionOptions }