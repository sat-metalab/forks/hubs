import { paths } from "../paths";
import { sets } from "../sets";
import { xforms } from "./xforms";
import { addSetsToBindings } from "./utils";

export const webSocketUserBindings = addSetsToBindings({
  [sets.global]: [
    {
      src: { value: paths.device.websocket.coords },
      dest: { value: "/var/websocketCoordsDelta" },
      xform: xforms.diff_vec2
    },
    {
      src: { value: "/var/websocketCoordsDelta" },
      dest: { x: "/var/websocketCoordsX", y: "/var/websocketCoordsY" },
      xform: xforms.split_vec2
    },
    {
      src: { value: "/var/websocketCoordsX" },
      dest: { value: "/var/websocketCoordsXScaled" },
      xform: xforms.scale(-Math.PI)
    },
    {
      src: { value: "/var/websocketCoordsY" },
      dest: { value: "/var/websocketCoordsYScaled" },
      xform: xforms.scale((2 * Math.PI) / 3)
    },
    {
      src: { x: "/var/websocketCoordsXScaled", y: "/var/websocketCoordsYScaled" },
      dest: { value: paths.actions.cameraDelta },
      xform: xforms.compose_vec2
    },
    {
      src: { value: paths.device.websocket.left_hand.matrix },
      dest: { value: paths.actions.leftHand.matrix },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.websocket.left_hand.pose },
      dest: { value: paths.actions.leftHand.pose },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.websocket.right_hand.matrix },
      dest: { value: paths.actions.rightHand.matrix },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.websocket.right_hand.pose },
      dest: { value: paths.actions.rightHand.pose },
      xform: xforms.copy
    },
    {
      src: {
        w: paths.device.websocket.gesture.forward,
        a: paths.device.websocket.gesture.strafeLeft,
        s: paths.device.websocket.gesture.backward,
        d: paths.device.websocket.gesture.strafeRight
      },
      dest: { vec2: "/var/websocket/arrows_vec2" },
      xform: xforms.wasd_to_vec2
    },
    {
      src: { value: "/var/websocket/arrows_vec2" },
      dest: { value: paths.actions.characterAcceleration },
      xform: xforms.normalize_vec2
    },
    {
      src: { value: paths.device.websocket.gesture.lookX },
      dest: { value: "/var/websocketLookXScaled" },
      xform: xforms.scale(0.01)
    },
    {
      src: { value: paths.device.websocket.gesture.lookY },
      dest: { value: "/var/websocketLookYScaled" },
      xform: xforms.scale(0.01)
    },
    {
      src: {
        x: "/var/websocketLookXScaled",
        y: "/var/websocketLookYScaled",
      },
      dest: { value: "/var/websocketLookVec" },
      xform: xforms.compose_vec2
    },
    {
      src: { 
        first: "/var/websocketLookVec",
        second: paths.actions.cameraDelta
      },
      dest: { value: paths.actions.cameraDelta },
      xform: xforms.add_vec2
    }
  ],
  [sets.rightCursorHoveringOnInteractable]: [],
  [sets.rightCursorHoveringOnUI]: [],
  [sets.rightCursorHoldingUI]: [],
  [sets.rightCursorHoveringOnVideo]: [],
  [sets.rightCursorHoldingInteractable]: [],
  [sets.rightHandTeleporting]: [],
  [sets.rightCursorHoldingPen]: [],
  [sets.rightCursorHoldingCamera]: []
});
