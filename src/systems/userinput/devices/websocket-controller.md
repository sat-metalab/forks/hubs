# WebSocket Controller
## Activate
WebSocket Controller is activated by an HTTP GET request with the parameter `wsdevice`.
This parameter accepts a JSON object with three options:
- "head": moves the head of the avatar
- "hands": moves the hand of the avatar
- "move": moves the whole avatar

With all the options activated, the GET request looks like this: `wsdevice={"move":true,"head":true,"hands":true}`

It is not necessary to write down all the options.

Example: `https://localhost:8080/hub.html?hub_id=smoke&wsdevice={"move":true,"head":false}`

## Move the Avatar
### Action Mode
To move the avatar, both elbows and both shoulders must approximately form a line together. All the gestures to move the avatar suppose these four parts of the body form a line. This unnatural position has been chosen to allow the arms to move mostly freely without risking to move the avatar accidentally.

### Moves
- Move forward: Any hand is pointing upward. The lower arm is perpendicular with the upper arm.
- Move backward: Any hand is pointing downward. The lower arm is perpendicular with the upper arm.
- Strafe left: The left hand is pointing left. The left arm is straight.
- Strafe right: The right hand is pointing right. The right arm is straight.
- Turn left: The left elbow is pointing down, while the right elbow is pointing up.
- Turn right: The left elbow is pointing up, while the right elbow is pointing down.

Combinaisons can be made. For example, it is possible to move forward, strafe right and turn left at the same time.

![WebSocketController](websocket-controller.svg)