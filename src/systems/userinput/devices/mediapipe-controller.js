import { paths } from "../paths";
import { Camera } from '@mediapipe/camera_utils/camera_utils';
import { FaceMesh } from '@mediapipe/face_mesh/face_mesh';

/*
    MediaPipe Face Mesh landmark indices can be found here:
    https://github.com/tensorflow/tfjs-models/blob/838611c02f51159afdd77469ce67f0e26b7bbb23/face-landmarks-detection/src/mediapipe-facemesh/keypoints.ts

    Annotated images for easy reference:
    https://github.com/ManuelTS/augmentedFaceMeshIndices
*/

// Source: https://stackoverflow.com/a/56539075
// TODO: Find a proper home for this helper code if we're going to use it
class Q {
    // Pairs of top & bottom for eyes
    static EYES = {
        RIGHT: {
            H: [361, 262],
            V: [385, 373],
        },
        LEFT: {
            H: [132,  32],
            V: [158, 144],
        },
    };

    static MOUTH = [11,  14];

    static normalizeAngle(angle){
        if (angle > 360)
            return angle - 360;
        if (angle < 0)
            return 360 + angle;
        else
            return angle;
    }

    static getAngleBetweenPoints(cx, cy, ex, ey){
        const dy = ey - cy;
        const dx = ex - cx;
        let theta = Math.atan2(dy, dx);
        theta *= 180 / Math.PI;
        return theta;
    }

    static angleBetween(p1, p2){
        return {
            x: this.normalizeAngle(this.getAngleBetweenPoints(p1.z, p1.x, p2.z, p2.x)),
            y: this.normalizeAngle(this.getAngleBetweenPoints(p1.z, p1.y, p2.z, p2.y)),
            z: this.normalizeAngle(this.getAngleBetweenPoints(p1.x, p1.y, p2.x, p2.y))
        }
    }
}

export class MediaPipeDevice {
    constructor() {
        this.videoElement = document.createElement('video');
        this.canvasElement = document.createElement('canvas');

        this.canvasElement.style.maxWidth = "100%";
        this.canvasElement.style.display = "block";
        this.canvasElement.style.position = "relative";
        this.canvasElement.style.left = "0";
        this.canvasElement.style.top = "0";
        this.canvasElement.style.width = "720px";
        this.canvasElement.style.height = "576px"

        this.canvasCtx = this.canvasElement.getContext('2d');

        this.controls = window;
        this.drawingUtils = window; 
        this.mpFaceMesh = window;

        this.rightEyeClosed = false;
        this.rightEyeRecent = [];
        this.leftEyeClosed = false;
        this.leftEyeRecent = [];
        this.mouthClosed = true;
        this.mouthRecent = [];

        this.noseTip = null;
        this.faceCentroid = null;
        this.headPose = {
            e: false, // Enable: disable upon initialization
            h: null,  // Horizontal: 'left', 'right', or null
            v: null,  // Vertical: 'up', 'down', or null
            m: 0.0,   // Magnitude: float
            a: null,  // Angle: object with x,y,z floats
        };

        this.MAX_RECENT_LENGTH = 5;

        // Member Functions
        this.onResults = this.onResults.bind(this)
        this.drawLine = this.drawLine.bind(this);
        this.drawDot = this.drawDot.bind(this);
        this.calcDistance = this.calcDistance.bind(this);
        this.drawEye = this.drawEye.bind(this);
        this.drawMouth = this.drawMouth.bind(this);
        this.calcCentroid = this.calcCentroid.bind(this);
        this.updateHeadPose = this.updateHeadPose.bind(this);
        this.isEyeClosed = this.isEyeClosed.bind(this);
        this.isLeftEyeClosed = this.isLeftEyeClosed.bind(this);
        this.isRightEyeClosed = this.isRightEyeClosed.bind(this);

        this.faceMesh = new FaceMesh({locateFile: (file) => {
            return `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh@0.4/${file}`;
        }});
        this.faceMesh.setOptions({
            selfieMode: true,
            model: 0, // Short range model for faces 2 meters from camera
            minDetectionConfidence: 0.65,
            refineLandmarks: true, // Requires more compute, but necessary for iris + blink detection
        });
        this.faceMesh.onResults(this.onResults);

        (async () => {
            await this.faceMesh.initialize();
        })();

        this.camera = new Camera(this.videoElement, {
            onFrame: async () => {
                await this.faceMesh.send({image: this.videoElement});
            },
            width: 1280,
            height: 720
        });
        this.camera.start();
    }
      
    onResults(results) {
        // Draw the overlays.
        this.canvasCtx.save();
        this.canvasCtx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
        this.canvasCtx.drawImage(
        results.image, 0, 0, this.canvasElement.width, this.canvasElement.height);
        if (results.multiFaceLandmarks.length > 0) {
            const L = results.multiFaceLandmarks.flat();

            const pTop    = L[167]; // Nasal bridge
            const pBottom = L[15];  // Bottom lip
            const pLeft   = L[122];
            const pRight  = L[351];
            const [cx, cy, cz] = this.calcCentroid([pTop, pBottom, pLeft, pRight]);
            this.faceCentroid = {
                x: cx,
                y: cy,
                z: 0,
                xpix: this.canvasElement.width  * cx,
                ypix: this.canvasElement.height * cy,
            }

            this.noseTip = L[3];
            this.noseTip.xpix = this.noseTip.x * this.canvasElement.width;
            this.noseTip.ypix = this.noseTip.y * this.canvasElement.height;

            if (window.APP.store.state.preferences.mediaPipeDrawNose) {
                this.drawLine(this.faceCentroid, this.noseTip);
                this.drawDot(this.faceCentroid, 'blue');
                this.drawDot(this.noseTip);
            }

            this.leftEyeClosed = this.isLeftEyeClosed(L);
            this.rightEyeClosed = this.isRightEyeClosed(L);

            const rightEyeMiddleTop = L[385];
            rightEyeMiddleTop.xpix = this.canvasElement.width * rightEyeMiddleTop.x;
            rightEyeMiddleTop.ypix = this.canvasElement.height * rightEyeMiddleTop.y;
            const leftEyeMiddleTop = L[158];
            leftEyeMiddleTop.xpix = this.canvasElement.width * leftEyeMiddleTop.x;
            leftEyeMiddleTop.ypix = this.canvasElement.height * leftEyeMiddleTop.y;
            if (window.APP.store.state.preferences.mediaPipeDrawEye) this.drawEye(rightEyeMiddleTop, this.rightEyeClosed);
            if (window.APP.store.state.preferences.mediaPipeDrawEye) this.drawEye(leftEyeMiddleTop, this.leftEyeClosed);

            this.mouthClosed = this.isMouthClosed(L);
            const mouthTop = L[12];
            mouthTop.xpix = this.canvasElement.width * mouthTop.x;
            mouthTop.ypix = this.canvasElement.height * mouthTop.y;
            if (window.APP.store.state.preferences.mediaPipeDrawMouth) this.drawMouth(mouthTop, this.mouthClosed);

            this.updateHeadPose();
        } else {
            // Disable control if landmarks not found
            this.headPose.v = null;
            this.headPose.h = null;
        }
        this.canvasCtx.restore();
    }

    drawDot(p, fillStyle = 'yellow'){
        this.canvasCtx.beginPath();
        this.canvasCtx.arc(p.xpix, p.ypix, 4.5, 0, 2 * Math.PI, true);
        this.canvasCtx.fillStyle = fillStyle;
        this.canvasCtx.fill();
    }

    drawLine(p1, p2, strokeStyle = 'gray') {
        this.canvasCtx.beginPath();
        this.canvasCtx.lineWidth = 2.5;
        this.canvasCtx.strokeStyle = strokeStyle;
        this.canvasCtx.moveTo(p1.xpix, p1.ypix);
        this.canvasCtx.lineTo(p2.xpix, p2.ypix);
        this.canvasCtx.stroke();
    }

    drawEye(p1, isClosed = false) {
        this.canvasCtx.fontSize = '1rem'
        this.canvasCtx.textBaseline = 'middle';
        this.canvasCtx.textAlign = 'center';
        this.canvasCtx.fillText(isClosed ? '🚫' : '👁️', p1.xpix, p1.ypix)
    }

    drawMouth(p1, isClosed = true) {
        this.canvasCtx.fontSize = '1rem'
        this.canvasCtx.textBaseline = 'middle';
        this.canvasCtx.textAlign = 'center';
        this.canvasCtx.fillText(isClosed ? '👄' : '⭕', p1.xpix, p1.ypix)
    }

    updateHeadPose() {
        this.headPose.e = true;
        this.headPose.m = this.calcDistance(this.faceCentroid, this.noseTip, true);
        this.headPose.a = Q.angleBetween(this.faceCentroid, this.noseTip);

        if (this.headPose.a.x <= window.APP.store.state.preferences.mediaPipeFaceLeftTrigger) {
            this.headPose.h = 'left';
        } else if (this.headPose.a.x >= window.APP.store.state.preferences.mediaPipeFaceRightTrigger) {
            this.headPose.h = 'right';
        } else {
            this.headPose.h = null;
        }

        if (this.headPose.a.y <= window.APP.store.state.preferences.mediaPipeFaceDownTrigger) {
            this.headPose.v = 'down';
        } else if (this.headPose.a.y >= window.APP.store.state.preferences.mediaPipeFaceUpTrigger) {
            this.headPose.v = 'up';
        } else {
            this.headPose.v = null;
        }
    }

    isEyeClosed(L, leftEyeChosen, debug = false) {
        const eye = leftEyeChosen ? Q.EYES.LEFT : Q.EYES.RIGHT;
        const [h1, h2] = eye.H.map(i => L[i]);
        const [v1, v2] = eye.V.map(i => L[i]);

        const dh = this.calcDistance(h1, h2);
        const dv = this.calcDistance(v1, v2);

        const r = dh / dv;

        if (leftEyeChosen) {
            if (this.leftEyeRecent.length > this.MAX_RECENT_LENGTH)
                this.leftEyeRecent.shift();
            this.leftEyeRecent.push(r);
        } else {
            if (this.rightEyeRecent.length > this.MAX_RECENT_LENGTH)
                this.rightEyeRecent.shift();
            this.rightEyeRecent.push(r);
        }

        const eyeAverage = leftEyeChosen
            ? ( this.leftEyeRecent.reduce((acc, cv) => acc + cv, 0)) / this.MAX_RECENT_LENGTH
            : (this.rightEyeRecent.reduce((acc, cv) => acc + cv, 0)) / this.MAX_RECENT_LENGTH;

        if (debug)
            console.log(`Average Ratio: ${eyeAverage}`)
        return eyeAverage >= window.APP.store.state.preferences.mediaPipeEyeCloseThreshold ? true : false;
    }

    isLeftEyeClosed(L) {
        return this.isEyeClosed(L, true);
    }

    isRightEyeClosed(L) {
        return this.isEyeClosed(L, false);
    }

    isMouthClosed(L, debug = false) {
        const [v1, v2] = Q.MOUTH.map(i => L[i]);
        const dv = this.calcDistance(v1, v2);

        if (this.mouthRecent.length > this.MAX_RECENT_LENGTH)
            this.mouthRecent.shift();
        this.mouthRecent.push(dv);

        const mouthAverage = this.mouthRecent.reduce((acc, cv) => acc + cv, 0) / this.MAX_RECENT_LENGTH;

        if (debug)
            console.log(`Mouth Vertical Distance: ${dv}`);
        return mouthAverage <= window.APP.store.state.preferences.mediaPipeMouthCloseThreshold ? true : false;
    }

    calcDistance(p1, p2, in3D = false) {
        const xQuantity = Math.pow((p2.x - p1.x), 2);
        const yQuantity = Math.pow((p2.y - p1.y), 2);
        const zQuantity = in3D ? Math.pow((p2.z - p1.z), 2) : 0;
        return(Math.sqrt(xQuantity + yQuantity + zQuantity));
    }


    calcCentroid(points) {
        const xAvg = (points.reduce((acc, cv) => acc + cv.x, 0)) / points.length;
        const yAvg = (points.reduce((acc, cv) => acc + cv.y, 0)) / points.length;
        const zAvg = (points.reduce((acc, cv) => acc + cv.z, 0)) / points.length;
        return [xAvg, yAvg, zAvg];
    }

    write(frame) {
        frame.setValueType(paths.device.mediapipe.rightEye, !this.rightEyeClosed);
        frame.setValueType(paths.device.mediapipe.leftEye, !this.leftEyeClosed);
        frame.setValueType(paths.device.mediapipe.mouth, !this.mouthClosed);

        if (this.headPose.e) {
            if (this.headPose.h === 'right') {
                frame.setValueType(paths.device.mediapipe.lookX, 1);
            } else if (this.headPose.h === 'left') {
                frame.setValueType(paths.device.mediapipe.lookX, -1);
            } else {
                frame.setValueType(paths.device.mediapipe.lookX, 0);
            }

            if (this.headPose.v === 'down') {
                frame.setValueType(paths.device.mediapipe.lookY, -1);
            } else if (this.headPose.v === 'up') {
                frame.setValueType(paths.device.mediapipe.lookY, 1);
            } else {
                frame.setValueType(paths.device.mediapipe.lookY, 0);
            }
        }
    }
}