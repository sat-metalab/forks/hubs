import { paths } from "../paths";
import { Pose } from "../pose";
import { waitForDOMContentLoaded } from "../../../utils/async-utils";

const ONES = new THREE.Vector3(1, 1, 1);
// Taken from vive-controller.js
const HAND_OFFSET = new THREE.Matrix4().compose(
  new THREE.Vector3(0.0, -0.2, 0.0),
  new THREE.Quaternion().setFromEuler(new THREE.Euler(-40 * THREE.Math.DEG2RAD, 0, 0)),
  ONES
);

const handHorizontalShiftAngle = 60.0 * THREE.Math.DEG2RAD;
const handVerticalShiftAngle = 45.0 * THREE.Math.DEG2RAD;
const shiftToElbow = 0.2;
const shiftBelowElbow = -0.1;

const m = new THREE.Matrix4();

function isLooselyEqual(a, b, precision=0.4) {
  if (a == b) return true; // prevents division per 0
  return Math.abs((a-b)/Math.max(a,b)) <= precision;
}

class Body {
  constructor() {
    this._positions = {}
  }

  changePositions(positions) {
    this._positions = positions
  }

  _getPosition(bodyPartName) {
    const pos = this._positions[bodyPartName]
    if (!pos) {
      return null;
    }
    return new THREE.Vector2(pos[0], pos[1]);
  }

  get headPos() {
    return this._getPosition("head");
  }
  get leftShoulderPos() {
    return this._getPosition("left_shoulder");
  }
  get rightShoulderPos() {
    return this._getPosition("right_shoulder");
  }
  get leftElbowPos() {
    return this._getPosition("left_elbow");
  }
  get rightElbowPos() {
    return this._getPosition("right_elbow");
  }
  get leftHandPos() {
    return this._getPosition("left_hand");
  }
  get rightHandPos() {
    return this._getPosition("right_hand");
  }
}

export class WebSocketDevice {
  constructor(params) {
    this.options = JSON.parse(params);
    this.leftHandMatrix = new THREE.Matrix4();
    this.rightHandMatrix = new THREE.Matrix4();
    this.sittingToStandingMatrix = new THREE.Matrix4().makeTranslation(0, 1.6, 0);
    this.body = new Body();

    this.socketClient = new WebSocket("ws://localhost:8765");
    this.socketClient.onmessage = event => {
      const message = JSON.parse(event.data);
      this.body.changePositions(message["hubs"]);
    };

    this.rayObjectRotation = new THREE.Quaternion();
    this.pose = new Pose();
    waitForDOMContentLoaded().then(() => {
      this.rayObject = document.querySelector("#player-right-controller").object3D;
      this.headObject3D = document.querySelector("#avatar-pov-node").object3D;
    });
  }

  _analyseGesture() {
    const leftElbowPos = this.body.leftElbowPos;
    const rightElbowPos = this.body.rightElbowPos;
    if (!leftElbowPos || !rightElbowPos) {
      return;
    }

    const leftShoulderPos = this.body.leftShoulderPos;
    const rightShoulderPos = this.body.rightShoulderPos;
    if (!leftShoulderPos || !rightShoulderPos) {
      return;
    }
    
    const leftUpperArmLenghtSquared = leftShoulderPos.distanceToSquared(leftElbowPos);
    const leftUpperArmLenght = Math.sqrt(leftUpperArmLenghtSquared);
    const rightUpperArmLenghtSquared = rightShoulderPos.distanceToSquared(rightElbowPos);
    const rightUpperArmLenght = Math.sqrt(rightUpperArmLenghtSquared);
    const clavicleLenght = leftShoulderPos.distanceTo(rightShoulderPos);

    const elbowsDist = leftElbowPos.distanceTo(rightElbowPos);
    // Checks both elbows approximately form a line with the shoulders
    if (!isLooselyEqual(leftUpperArmLenght+rightUpperArmLenght+clavicleLenght, elbowsDist)
      || !isLooselyEqual(leftUpperArmLenght, clavicleLenght)
      || !isLooselyEqual(rightUpperArmLenght, clavicleLenght)
    ) {
      return; // they don't. Nothing can be done.
    }
    
    const leftArmGesture = this._analyseArmGesture(this.body.leftHandPos,
                                                   leftElbowPos,
                                                   leftShoulderPos,
                                                   leftUpperArmLenght,
                                                   leftUpperArmLenghtSquared);
    const rightArmGesture = this._analyseArmGesture(this.body.rightHandPos,
                                                    rightElbowPos,
                                                    rightShoulderPos,
                                                    rightUpperArmLenght,
                                                    rightUpperArmLenghtSquared);

    return {
      "forward": (leftArmGesture != rightArmGesture) && (leftArmGesture == "up" || rightArmGesture == "up"), // only one hand up
      "backward": (leftArmGesture != rightArmGesture) && (leftArmGesture == "down" || rightArmGesture == "down"), // only one hand down
      "strafeLeft": leftArmGesture == "straight",
      "strafeRight": rightArmGesture == "straight",
      "lookX": this._getLookXValue(leftElbowPos, rightElbowPos, clavicleLenght),
      "lookY": 0 // this._getLookYValue(leftArmGesture, rightArmGesture) // Deactivated. Not good enough.
    };
  }

  _analyseArmGesture(handPos, elbowPos, shoulderPos, upperArmLength, upperArmLengthSquared) {
    if (!handPos) {
      return null;
    }

    const shoulderToHandDistSquared = handPos.distanceToSquared(shoulderPos);
    const lowerArmLengthSquared = handPos.distanceToSquared(elbowPos);
    // Checks upper arm and lower arm are approximately 90°
    if (isLooselyEqual(upperArmLengthSquared+lowerArmLengthSquared, shoulderToHandDistSquared)) {
      if (handPos.y > elbowPos.y) {
        return "up";
      } else {
        return "down";
      }
    }

    const shoulderToHandDist = Math.sqrt(shoulderToHandDistSquared);
    const lowerArmLength = Math.sqrt(lowerArmLengthSquared);
    // Checks arm is approximately straight
    if (isLooselyEqual(upperArmLength+lowerArmLength, shoulderToHandDist)) {
      return "straight";
    }

    return null;
  }

  _getLookXValue(leftElbowPos, rightElbowPos, clavicleLenght) {
    // clavicleLength is an arbitrary value to compare the relative difference in height of both elbows
    if (Math.abs(leftElbowPos.y-rightElbowPos.y) > clavicleLenght*0.7) {
      return leftElbowPos.y < rightElbowPos.y ? 1 : -1;
    }
    return 0;
  };
  
  _getLookYValue(leftArmGesture, rightArmGesture) {
    if (leftArmGesture == "up" && rightArmGesture == "up") {
      return 1; // look up
    } else if (leftArmGesture == "down" && rightArmGesture == "down") {
      return -1; // look down
    }
    return 0;
  }

  write(frame) {
    // Moving whole body
    if (this.options.move) {
      const gesture = this._analyseGesture();
      if (gesture) {
        Object.entries(gesture)
        .forEach(([gestureName, value]) => {
          frame.setValueType(paths.device.websocket.gesture[gestureName], value);
        });
      }
    }

    // Moving members
    if (!this.rayObject || !this.headObject3D) return;
    if (!this.options.head && !this.options.hands) return;
    const leftShoulder = this.body.leftShoulderPos;
    const rightShoulder = this.body.rightShoulderPos;
    if (!leftShoulder || !rightShoulder) {
      return;
    }

    const neckPos = new THREE.Vector2(
      (leftShoulder.x+rightShoulder.x)/2,
      (leftShoulder.y+rightShoulder.y)/2
    )
    
    const leftShoulderPos = new THREE.Vector2(leftShoulder.x, leftShoulder.y).sub(neckPos);
    const rightShoulderPos = new THREE.Vector2(rightShoulder.x, rightShoulder.y).sub(neckPos);

    // Compute vectors for the "body reference space"
    const xVec = rightShoulderPos.sub(leftShoulderPos).normalize();
    const yVec = new THREE.Vector2(-xVec.y, xVec.x).normalize();
    const basis = new THREE.Matrix3().set(xVec.x, yVec.x, 0.0, xVec.y, yVec.y, 0.0, 0.0, 0.0, 0.0);

    if (this.options.head) {
      const head = this.body.headPos
      if (head) {
        // Head position is not centered around Y, this is easier to handle
        const headPos = new THREE.Vector2(head.x, head.y).sub(neckPos).applyMatrix3(basis);
        frame.setVector2(paths.device.websocket.coords, headPos.x, headPos.y);
      }
    }

    this.rayObject.updateMatrixWorld();
    this.rayObjectRotation.setFromRotationMatrix(m.extractRotation(this.rayObject.matrixWorld));
    this.pose.position.setFromMatrixPosition(this.rayObject.matrixWorld);
    this.pose.direction.set(0, 0, -1).applyQuaternion(this.rayObjectRotation);
    this.pose.fromOriginAndDirection(this.pose.position, this.pose.direction);

    this.headObject3D.updateMatrices();

    if (this.options.hands) {
      this._writeLeftHand(frame, this.body.leftHandPos, rightShoulderPos, neckPos, basis);
      this._writeRightHand(frame, this.body.rightHandPos, leftShoulderPos, neckPos, basis);
    }

  }

  _writeLeftHand(frame, leftHandPos, rightShoulderPos, neckPos, basis) {
    if (leftHandPos) {
      frame.setPose(paths.device.websocket.left_hand.pose, this.pose);

      const handPos = new THREE.Vector2(leftHandPos[0], leftHandPos[1])
        .sub(neckPos)
        .sub(rightShoulderPos)
        .applyMatrix3(basis);

      // Up/down rotation, then left/right
      const handVerticalQuat = new THREE.Quaternion().setFromEuler(
        new THREE.Euler(handPos.y + handHorizontalShiftAngle, 0.0, 0.0)
      );
      const handHorizontalQuat = new THREE.Quaternion().setFromEuler(
        new THREE.Euler(0.0, -handPos.x - handVerticalShiftAngle, 0.0, "XYZ")
      );

      const handQuat = new THREE.Quaternion().multiplyQuaternions(handVerticalQuat, handHorizontalQuat);
      frame.setMatrix4(
        paths.device.websocket.left_hand.matrix,
        this.leftHandMatrix
          .compose(
            new THREE.Vector3(-shiftToElbow, shiftBelowElbow, 0.0).applyQuaternion(this.headObject3D.quaternion),
            new THREE.Quaternion().multiplyQuaternions(this.headObject3D.quaternion, handQuat),
            ONES
          )
          .premultiply(this.sittingToStandingMatrix)
          .multiply(HAND_OFFSET)
      );
    } else {
      frame.unset(paths.device.websocket.left_hand.matrix);
    }
  }
  
  _writeRightHand(frame, rightHandPos, leftShoulderPos, neckPos, basis) {
      if (rightHandPos) {
      frame.setPose(paths.device.websocket.right_hand.pose, this.pose);

      const handPos = new THREE.Vector2(rightHandPos.x, rightHandPos.y)
        .sub(neckPos)
        .sub(leftShoulderPos)
        .applyMatrix3(basis);

      // Up/down rotation, then left/right
      const handVerticalQuat = new THREE.Quaternion().setFromEuler(
        new THREE.Euler(handPos.y + handHorizontalShiftAngle, 0.0, 0.0)
      );
      const handHorizontalQuat = new THREE.Quaternion().setFromEuler(
        new THREE.Euler(0.0, -handPos.x + handVerticalShiftAngle, 0.0, "XYZ")
      );

      const handQuat = new THREE.Quaternion().multiplyQuaternions(handVerticalQuat, handHorizontalQuat);
      frame.setMatrix4(
        paths.device.websocket.right_hand.matrix,
        this.rightHandMatrix
          .compose(
            new THREE.Vector3(shiftToElbow, shiftBelowElbow, 0.0).applyQuaternion(this.headObject3D.quaternion),
            new THREE.Quaternion().multiplyQuaternions(this.headObject3D.quaternion, handQuat),
            ONES
          )
          .premultiply(this.sittingToStandingMatrix)
          .multiply(HAND_OFFSET)
      );
    } else {
      frame.unset(paths.device.websocket.right_hand.matrix);
    }
  }
}
